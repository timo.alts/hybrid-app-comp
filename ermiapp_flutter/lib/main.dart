import 'package:flutter/material.dart';

import 'screen/home.dart';
import 'screen/scanscreen.dart';
import 'service/database.dart';

void main() async {
  await DBProvider().initDB();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ermiapp',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        // When we navigate to the "/" route, build the FirstScreen Widget
        '/': (context) => Home(title: 'ermiapp'),
        // When we navigate to the "/second" route, build the SecondScreen Widget
        '/second': (context) => ScanScreen(),
      },
    );
  }
}
