import 'package:ermiapp_flutter/screen/widget/api_consumer_widget.dart';
import 'package:ermiapp_flutter/screen/widget/local_db_consumer_widget.dart';
import 'package:flutter/material.dart';

import 'widget/placeholder_widget.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    ApiConsumer(),
    LocalConsumer(),
    PlaceholderWidget(Colors.orange)
  ];

  void _navigateToScreen() {
    Navigator.pushNamed(context, '/second');
  }

  void _onItemTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _children[_currentIndex],
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _navigateToScreen,
        tooltip: 'Secret',
        label: Text('Secret'),
      ), // This trailing comma makes auto-formatting nicer for build methods.
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.wb_sunny),
            title: Text('Api'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.computer),
            title: Text('Lokal'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera),
            title: Text('Scan'),
          ),
        ],
        currentIndex: _currentIndex,
        selectedItemColor: Colors.cyan[600],
        onTap: _onItemTapped,
      ),
    );
  }
}
