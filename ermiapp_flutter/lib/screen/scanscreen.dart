import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'widget/placeholder_widget.dart';

import 'package:barcode_scan/barcode_scan.dart';

class ScanScreen extends StatefulWidget {
  @override
  _ScanScreenState createState() => _ScanScreenState();
}

class _ScanScreenState extends State<ScanScreen> {
  String result = "Empty";
  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult;
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        print("Camera Permission Denied!");
        setState(() {
          result = "Camera Permission Denied!";
        });
      } else {
        setState(() {
          result = "Unknown Error, $ex";
        });
      }
    } on FormatException {
      setState(() {
          result = "Back Button Pressed";
        });
    } catch(ex){
      setState(() {
          result = "Unknown Error, $ex";
        });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScanScreen Screen'),
      ),
      body: Center(
        child: Text(result),
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.camera_alt),
        label: Text('Scan'),
        onPressed: _scanQR,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
