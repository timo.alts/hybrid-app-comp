import 'dart:convert';

import 'package:ermiapp_flutter/model/post.dart';
import 'package:ermiapp_flutter/service/placeholder_service.dart';
import 'package:flutter/material.dart';

class ApiConsumer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<List<Post>>(
        future: getAllPosts(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return createPostListView(context, snapshot);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }

          // By default, show a loading spinner
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget createPostListView(BuildContext context, AsyncSnapshot snapshot) {
    List<Post> posts = snapshot.data;
    return ListView.builder(
      itemCount: posts.length,
      itemBuilder: (BuildContext context, int index) {
        return new Column(children: <Widget>[
          ListTile(
            subtitle: Text(posts[index].body),
            title: Text(
              posts[index].title,
            ),
            contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          ),
          Divider(
            height: 2.0,
          )
        ]);
      },
    );
  }
}
