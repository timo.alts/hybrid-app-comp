import 'dart:io';
import 'package:ermiapp_flutter/model/post.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

Database db;

class DBProvider {
  static const postTable = 'posts';
  static const id = 'id';
  static const userId = 'userId';
  static const title = 'title';
  static const body = 'body';

  Future<String> _databasePath(String dbName) async {
    final dbp = await getDatabasesPath();
    final path = join(dbp, dbName);
    if (await Directory(dirname(path)).exists()) {
    } else {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }

  Future<void> _createPostTable(Database db) async {
    final postSql = '''CREATE TABLE $postTable 
    (
      $id INTEGER PRIMARY KEY,
      $userId INTEGER,
      $title TEXT,
      $body TEXT
    )
    ''';
    await db.execute(postSql);
  }

  Future<void> _onCreate(Database db, int version) async {
    await _createPostTable(db);
  }

  Future<void> initDB() async {
    final String path = await _databasePath('ermiappLokalDb');
    db = await openDatabase(path, version: 1, onCreate: _onCreate);

    print(db);
  }
}

class DbServicePosts {
  static Future<List<Post>> getAllPosts() async {
    final sql = '''SELECT * 
    FROM ${DBProvider.postTable}
    ''';
    final data = await db.rawQuery(sql);

    List<Post> posts = List();

    for (final node in data) {
      final post = Post.fromJson(node);
      posts.add(post);
    }
    return posts;
  }

  static Future<void> addPost(Post post) async {
    final sql = '''INSERT OR IGNORE INTO ${DBProvider.postTable}
      (
        ${DBProvider.id},
        ${DBProvider.userId},
        ${DBProvider.title},
        ${DBProvider.body}
      )
      VALUES
      (?,?,?,?);
    ''';
    List<dynamic> params = [post.id, post.userId, post.title, post.body];
    final result = await db.rawInsert(sql, params);
    print(result);
  }
}
