import 'package:ermiapp_flutter/model/post.dart';
import 'package:ermiapp_flutter/service/database.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'dart:async';
import 'dart:io';

String url = 'https://jsonplaceholder.typicode.com/posts';

bool updatedAfterLaunch = false;

Future<List<Post>> getAllPosts() async {
  final response = await http.get(url);
  print(response.body);
  List<Post> posts = allPostsFromJson(response.body);

  //store in local db
  if (!updatedAfterLaunch) {
    posts.forEach((p) {
      DbServicePosts.addPost(p);
    });
  }
  updatedAfterLaunch = true;

  return posts;
}

Future<Post> getPost() async {
  final response = await http.get('$url/1');
  return postFromJson(response.body);
}

Future<http.Response> createPost(Post post) async {
  final response = await http.post('$url',
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: ''
      },
      body: postToJson(post));
  return response;
}
