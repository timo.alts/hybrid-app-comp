import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocaldbService {
  private postTable = 'posts';
  private id = 'id';
  private userId = 'userId';
  private title = 'title';
  private body = 'body';

  sqlstorage: any = null;

  private ldb = null;

  public posts = new BehaviorSubject<any>([])

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
  ) {
  }

  initDb() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.ldb = db;
        let sql = `CREATE TABLE IF NOT EXISTS ${this.postTable}
            (
              ${this.id} INTEGER PRIMARY KEY,
              ${this.userId} INTEGER,
              ${this.title} TEXT,
              ${this.body} TEXT
            )`;
        db.executeSql(sql, [])
          .then(() => console.log('Executed SQL'))
          .catch(e => console.log(e));
      })
      .catch(e => console.log(e));
  }

  storePosts(posts) {
    console.log("STORE", posts);
    for (let post of posts) {
      let query = `INSERT OR IGNORE INTO ${this.postTable}
      (
        ${this.id},
        ${this.userId},
        ${this.title},
        ${this.body}
      )
      VALUES
      (?,?,?,?);
    `;
      let values = [post.id, post.userId, post.title, post.body];
      this.ldb.executeSql(query, values).then(console.log);
    }
  }

  getPosts() {
    this.ldb.executeSql(`SELECT * FROM ${this.postTable};`,{}).then((r) => {
      let ps = [];
      if (r.rows.length > 0) {
        for (var i = 0; i < r.rows.length; i++) {
          ps.push(r.rows.item(i));
        }
      }
      this.posts.next(ps);
    }).catch((e) => console.log(e));
  }
}
