import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecretPage } from './secret.page';

describe('SecretPage', () => {
  let component: SecretPage;
  let fixture: ComponentFixture<SecretPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
