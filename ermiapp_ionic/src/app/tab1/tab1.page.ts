import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { LocaldbService } from '../localdb.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  posts = new BehaviorSubject<any>(null);
  constructor(
    private http: HttpClient,
    private ldb: LocaldbService,
  ) {
  }

  ngOnInit() {
    this.fetchPosts();
  }

  async fetchPosts() {
    let fepo = await this.http.get("https://jsonplaceholder.typicode.com/posts").toPromise().catch(() => console.log("Fetch Error"));
    this.ldb.storePosts(fepo);
    this.posts.next(fepo);
  }
}
