import { Component } from '@angular/core';
import { LocaldbService } from '../localdb.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  posts: BehaviorSubject<any>;

  constructor(
    private ldb: LocaldbService,
  ) {
    this.posts = ldb.posts;
    this.getPosts();
  }

  getPosts() {
    this.ldb.getPosts();
  }

}
